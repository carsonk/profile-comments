<?php
/**
*
* Profile Comments
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace forumpromotion\profilecomments\controller;

class manage_controller
{
	protected $auth;
	protected $db;
	protected $request;
	protected $user;

	protected $root_path;
	protected $php_ext;

	protected $pc_table;
	protected $pr_table;

	/*
	* Constructor
	*
	* @param \phpbb\auth\auth $auth               
	* @param \phpbb\db\driver\driver_interface $db 
	* @param \phpbb\user $user
	* @param string $profile_comments_table
	* @param string $root_path
	* @param string $php_ext
	*/
	public function __construct(
		\phpbb\auth\auth $auth, 
		\phpbb\db\driver\driver_interface $db, 
		\phpbb\request\request $request, 
		\phpbb\user $user, 
		$profile_comments_table, 
		$profile_replies_table,
		$root_path, 
		$php_ext
	)
	{
		$this->auth = $auth;
		$this->db = $db;
		$this->request = $request;
		$this->user = $user;

		$this->root_path = $root_path;
		$this->php_ext = $php_ext;

		$this->pc_table = $profile_comments_table;
		$this->pr_table = $profile_replies_table;

		$this->user->add_lang_ext('forumpromotion/profilecomments', 'profilecomments_manage');
	}

	/**
	* Makes an edit to a specified comment, if the user has permission.
	* 
	* @param int  $comment_id  The id of the comment to edit.
	*/
	public function edit($comment_id)
	{
		if(!$this->auth->acl_get('u_procom_edit') && !$this->auth->acl_get('m_procom_edit'))
		{
			trigger_error('PROCOM_EDIT_NO_PERMISSION');
		}

		$sql_ary = array(
			'SELECT' => 'comment_user_id_from, comment_user_id_to, comment_message, comment_soft_deleted',
			'FROM' => array($this->pc_table => 'p'),
			'WHERE' => 'comment_id = ' . (int) $comment_id,
		);
		$sql = $this->db->sql_build_query('SELECT', $sql_ary);
		$result = $this->db->sql_query($sql);
		$row = $this->db->sql_fetchrow($result);
		$this->db->sql_freeresult($result);

		if(!$row)
		{
			trigger_error('PROCOM_MESSAGE_NOT_EXISTS');
		}

		if(
			!$this->auth->acl_get('m_procom_edit') 
			&& $row['comment_user_id_from'] != $this->user->data['user_id']
		)
		{
			trigger_error('PROCOM_EDIT_NO_PERMISSION');
		}

		// Comment is soft deleted and user does not have mod edit permission.
		if($row['comment_soft_deleted'] && !$this->auth->acl_get('m_procom_edit'))
		{
			trigger_error('PROCOM_EDIT_NO_PERMISSION');
		}

		if(!$this->request->is_set_post('submit') || !$this->request->is_set_post('message'))
		{
			trigger_error('PROCOM_EDIT_NO_POSTED_DATA');
		}

		$new_message = $this->request->variable('message', '');
		$bb_uid = $bb_bitfield = '';
		$flags = 0;
		generate_text_for_storage($new_message, $bb_uid, $bb_bitfield, $flags, TRUE, TRUE, TRUE);

		$new_message_data = array(
			'comment_message'       => $new_message,
			'bbcode_bitfield'       => $bb_bitfield,
			'bbcode_uid'            => $bb_uid,
		);
		$sql = 'UPDATE ' . $this->pc_table . ' SET ' . $this->db->sql_build_array('UPDATE', $new_message_data) . ' WHERE comment_id = ' . (int) $comment_id;
		$this->db->sql_query($sql);

		$params = 'mode=viewprofile&amp;u=' . $row['comment_user_id_to'];
		$redirect_url = append_sid($this->root_path . 'memberlist.' . $this->php_ext, $params);

		$redirect_link = '<br /><br /><a href="' . $redirect_url . '">' . $this->user->lang('PROCOM_PROFILE_RETURN') . '</a>';
		trigger_error($this->user->lang('PROCOM_SUCCESSFUL_EDIT') . $redirect_link);
	}

	/**
	* Gets raw message for comment.
	*
	* @param int  $comment_id   The id of the comment that users want to grab.
	*/
	public function get_raw_message($comment_id)
	{
		$json_response = new \phpbb\json_response();

		$comment_id = (int) $comment_id;
		$is_ajax = $this->request->is_ajax();

		if(!$is_ajax)
		{
			$this->trigger_error_ajax('PROCOM_EDIT_GET_MUST_AJAX');
		}

		$sql = 'SELECT comment_message, bbcode_bitfield, bbcode_uid
			FROM ' . $this->pc_table . '
			WHERE comment_id = ' . $comment_id;
		$result = $this->db->sql_query($sql);
		$row = $this->db->sql_fetchrow($result);

		if(!$row)
		{
			$this->trigger_error_ajax('PROCOM_MESSAGE_NOT_EXISTS');
		}

		$message = generate_text_for_edit($row['comment_message'], $row['bbcode_uid'], 7);

		$data = array(
			'SUCCESS' => TRUE,
			'MESSAGE' => $message['text'],
			'COMMENT_ID' => $comment_id,
		);
		$json_response->send($data);
	}	

	/**
	* Handles requests to delete or restore comments.
	*
	* @param int   The id of the comment.
	* @param bool  (optional) Restores posts instead of soft deletes.
	*/
	public function delete($comment_id, $restore = FALSE)
	{
		$has_permission = FALSE;
		$comment_id = (int) $comment_id;

		$user_id = $this->user->data['user_id'];

		$sql = 'SELECT comment_user_id_from, comment_user_id_to
			FROM ' . $this->pc_table . '
			WHERE comment_id = ' . $comment_id;
		$result = $this->db->sql_query($sql);
		$row = $this->db->sql_fetchrow($result);
		$this->db->sql_freeresult($result);

		if($this->auth->acl_get('m_procom_soft_del'))
		{
			$has_permission = TRUE;
		}
		else
		{
			if($row['comment_user_id_from'] == $user_id
				|| $row['comment_user_id_to'] == $user_id)
			{
				$has_permission = TRUE;
			}
		}

		if(!$row)
		{
			trigger_error('PROCOM_MESSAGE_NOT_EXISTS');
		}

		$profile_url = append_sid($this->root_path . 'memberlist.' . $this->php_ext, 'mode=viewprofile&u=' . $row['comment_user_id_to']);

		if($has_permission)
		{
			if(confirm_box(TRUE))
			{
				$new_sd_value = ($restore) ? 0 : 1;
				$sql = 'UPDATE ' . $this->pc_table . '
					SET comment_soft_deleted = ' . $new_sd_value . '
					WHERE comment_id = ' . $comment_id;
				$this->db->sql_query($sql);
			}
			else
			{
				$s_hidden_fields = build_hidden_fields(array(
		            'submit'    => true,
	            ));

				$message = ($restore) ? 'PROCOM_S_RESTORE' : 'PROCOM_S_DEL';
		        confirm_box(false, $message, $s_hidden_fields);
			}
		}
		else
		{
			trigger_error($this->user->lang('PROCOM_DELETE_NO_PERMISSION'));
		}

		redirect($profile_url);
	}

	public function delete_reply($reply_id, $restore = FALSE)
	{
		// TODO: Fix language keys to be more appropriate for replies.

		$has_permission = FALSE;
		$reply_id = (int) $reply_id;

		$user_id = $this->user->data['user_id'];

		$sql_ary = array(
			'SELECT' => 'r.reply_user_id_from, r.comment_id, r.reply_soft_deleted, c.comment_user_id_to, c.comment_user_id_from',
			'FROM' => array($this->pr_table => 'r'),
			'LEFT_JOIN' => array(
				array(
					'FROM' => array($this->pc_table => 'c'),
					'ON' => 'r.comment_id = c.comment_id'
				),
			),
			'WHERE' => 'r.reply_id = ' . $reply_id
		);

		$sql = $this->db->sql_build_query('SELECT', $sql_ary);
		$result = $this->db->sql_query($sql);
		$row = $this->db->sql_fetchrow($result);
		$this->db->sql_freeresult($result);

		if($this->auth->acl_get('m_procom_soft_del'))
		{
			$has_permission = TRUE;
		}
		else
		{
			if(
				$row['comment_user_id_from'] == $user_id
				|| $row['comment_user_id_to'] == $user_id
				|| $row['reply_user_id_from'] == $user_id
			)
			{
				$has_permission = TRUE;
			}
		}

		if(!$row)
		{
			trigger_error('PROCOM_MESSAGE_NOT_EXISTS');
		}

		$profile_url = append_sid($this->root_path . 'memberlist.' . $this->php_ext, 'mode=viewprofile&u=' . $row['comment_user_id_to']);

		if($has_permission)
		{
			if(confirm_box(TRUE))
			{
				$new_sd_value = ($restore) ? 0 : 1;
				$sql = 'UPDATE ' . $this->pr_table . '
					SET reply_soft_deleted = ' . $new_sd_value . '
					WHERE reply_id = ' . $reply_id;
				$this->db->sql_query($sql);
			}
			else
			{
				$s_hidden_fields = build_hidden_fields(array(
		            'submit'    => true,
	            ));

				$message = ($restore) ? 'PROCOM_S_RESTORE' : 'PROCOM_S_DEL';
		        confirm_box(false, $message, $s_hidden_fields);
			}
		}
		else
		{
			trigger_error($this->user->lang('PROCOM_DELETE_NO_PERMISSION'));
		}

		redirect($profile_url);
	}

	/**
	* Handles requests to restore deleted comments. Wrapper for delete with restore param.
	*
	* @param $comment_id
	*/
	public function restore($comment_id)
	{
		$this->delete($comment_id, TRUE);
	}

	/**
	* Handles restoration of replies.
	*
	* @param $reply_id  The id of the reply.
	*/
	public function restore_reply($reply_id)
	{
		$this->delete_reply($reply_id, TRUE);
	}

	/**
	* Dynamically triggers an error based on whether or not the request is AJAX based.
	*
	* @param string $error_message_lang_key   The language key for the error message.
	*/
	private function trigger_error_ajax($error_message_lang_key)
	{
		$this->user->lang($error_message_lang_key);

		if($this->request->is_ajax())
		{
			$data = array('ERROR_MSG' => $error_message);
		    $json_response = new \phpbb\json_response();
		    $json_response->send($data);
		}
		else
		{
			trigger_error($error_message_lang_key);
		}

		exit();
	}
}