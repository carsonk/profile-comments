CREATE TABLE IF NOT EXISTS `phpbb_comment` (
  `comment_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `comment_date` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment_poster_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `comment_text` text COLLATE utf8_bin NOT NULL,
  `comment_to_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `comment_author` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `bbcode_bitfield` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `bbcode_uid` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `bbcode_options` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `enable_smilies` tinyint(4) NOT NULL DEFAULT '1',
  `enable_bbcode` tinyint(4) NOT NULL DEFAULT '1',
  `enable_magic_url` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`comment_id`),
  KEY `comment_to_id` (`comment_to_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=31817 ;