<?php
/**
*
* Profile Comments
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace forumpromotion\profilecomments\migrations\v1;

class m1_schema_foundations extends \phpbb\db\migration\migration
{
	public function update_schema()
	{
		return array(
			'add_tables' => array(

				$this->table_prefix . 'profile_comments' => array(
					'COLUMNS' => array(
						'comment_id'               => array('UINT', NULL, 'auto_increment'),
						'comment_user_id_from'     => array('UINT', 0),
						'comment_user_id_to'       => array('UINT', 0),
						'comment_message'          => array('TEXT_UNI', ''),
						'bbcode_bitfield'          => array('VCHAR:255', ''),
						'bbcode_uid'               => array('VCHAR:8', ''),
						'comment_soft_deleted'     => array('BOOL', 0),
						'comment_time'             => array('TIMESTAMP', 0),
					),
					'PRIMARY_KEY' => 'comment_id',
					'KEYS' => array(
						'usr_frm' => array('INDEX', 'comment_user_id_from'),
						'usr_to'  => array('INDEX', 'comment_user_id_to'),
						'sft_del' => array('INDEX', 'comment_soft_deleted'),
					)
				),

				$this->table_prefix . 'profile_replies' => array(
					'COLUMNS' => array(
						'reply_id'           => array('UINT', NULL, 'auto_increment'),
						'comment_id'         => array('UINT', 0),
						'reply_user_id_from' => array('UINT', 0),
						'reply_message'      => array('TEXT_UNI', ''),
						'bbcode_bitfield'    => array('VCHAR:255', ''),
						'bbcode_uid'         => array('VCHAR:8', ''),
						'reply_soft_deleted' => array('BOOL', 0),
						'reply_time'         => array('TIMESTAMP', 0),
					),
					'PRIMARY_KEY' => 'reply_id',
					'KEYS' => array(
						'rcid' => array('INDEX', 'comment_id'),
						'rfid' => array('INDEX', 'reply_user_id_from'),
						'rdel'  => array('INDEX', 'reply_soft_deleted'),
					)
				),

			)
		);
	}

	public function revert_schema()
	{
		return array(
			'drop_tables' => array(
				$this->table_prefix . 'profile_comments'
			)
		);
	}
}