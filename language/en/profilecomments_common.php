<?php
/**
*
* Profile Comments
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'PROFILE_COMMENT'  => 'Profile Comment',
	'PROFILE_COMMENTS' => 'Profile Comments',

	'PROCOM_POST_COMMENT'           => 'Post Comment',
	'NOTIFICATION_PROFILE_COMMENT'  => '%s posted a comment to your profile.',
	'NOTIFICATION_PROFILE_COMMENT_OPTION' => 'Someone posts a comment to your profile',
	'NOTIFICATION_PROFILE_REPLY' => '%s posted a reply to a comment.',
	'NOTIFICATION_PROFILE_REPLY_OPTION' => 'Someone posts a reply to one of your comments or to a comment on your profile',

	'PROCOM_EDIT' => 'Edit',
));
