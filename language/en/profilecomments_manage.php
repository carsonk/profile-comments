<?php
/**
*
* Profile Comments
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'PROFILE_COMMENT'  => 'Profile Comment',
	'PROFILE_COMMENTS' => 'Profile Comments',

	'PROCOM_MESSAGE_NOT_EXISTS' => 'That message does not exist.',

	'PROCOM_EDIT_NO_PERMISSION' => 'You do not have permission to edit that comment.',
	'PROCOM_EDIT_NO_POSTED_DATA' => 'No data was given for editing.',
	'PROCOM_SUCCESSFUL_EDIT' => 'Message was successfully edited!',
	'PROCOM_EDIT_GET_MUST_AJAX' => 'Retrieving message for editing must be done via AJAX.',
	'PROCOM_MESSAGE_NOT_EXISTS' => 'That message does not exist.',
	
	'PROCOM_POST_COMMENT'     => 'Post Comment',
	'PROCOM_RESTORE'          => 'Restore',
	'PROCOM_SOFT_DELETED'     => 'Soft Deleted',

	'PROCOM_AJAX_ISSUE' => 'There was an AJAX issue.',

	'PROCOM_IS_SOFT_DELETED'      => 'This comment has been soft deleted.',
	'PROCOM_S_DEL'                => 'Soft Delete',
	'PROCOM_S_RESTORE'            => 'Restore Soft Delted Comment',
	'PROCOM_S_DEL_CONFIRM'        => 'Are you sure you would like to soft delete this comment? Note that the comment will still be visible to the board\'s moderators.',
	'PROCOM_S_RESTORE_CONFIRM'    => 'Are you sure you would like to restore this comment?',
	'PROCOM_DELETE_NO_PERMISSION' => 'You do not have permission to delete comments.',

	'PROCOM_PROFILE_RETURN' => 'Return to user\'s profile',
));
