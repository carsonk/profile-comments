(function ($) { 
	$(document).ready(function() {
		$(".procom-edit").click(function(e) {
			e.preventDefault();

			var raw_get_url = $(this).attr("href");

			if(raw_get_url)
			{
				$.post(raw_get_url, {}, function(data)
				{
					if(data.SUCCESS) {
						var raw_message = data.MESSAGE;

						$("#procom_" + data.COMMENT_ID).val(raw_message);

						// Fix any other opened stuff.
						$(".profile-comment-body-message-text").slideDown();
						$(".profile-comments-editor").slideUp();

						// Open current comment's editor and hide text.
						$("#procom_text_" + data.COMMENT_ID).slideUp();
						$("#procom_editor_" + data.COMMENT_ID).slideDown();
					} else if(data.ERROR_MSG) {
						alert(data.ERROR_MSG);
						$(".profile-comment-body-message-text").slideDown();
					} else {
						alert("{L_PROCOM_AJAX_ISSUE}");
						$(".profile-comment-body-message-text").slideDown();
					}
				}, "json");
			}

			return false;
		});
	});
})(jQuery);