<?php
/**
*
* Profile Comments
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace forumpromotion\profilecomments\event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class memberlist_listener implements EventSubscriberInterface
{
	/** @var \phpbb\auth\auth */
	protected $auth;
	/** @var \phpbb\db\driver\driver_interface */
	protected $db;
	/** @var \phpbb\controller\helper */
	protected $helper;
	/** @var \phpbb\notification\manager */
	protected $notification_manager;
	/** @var \phpbb\pagination **/
	protected $pagination;
	/** @var \phpbb\request\request */
	protected $request;
	/** @var \phpbb\template\template */
	protected $template;
	/** @var \phpbb\user */
	protected $user;

	/** @var string */
	protected $pc_table;
	/** @var string */
	protected $pc_replies_table;

	/** @var string */
	protected $root_path;
	/** @var string */
	protected $php_ext;

	/**
	* Constructor
	*/
	public function __construct(\phpbb\auth\auth $auth, \phpbb\db\driver\driver_interface $db, \phpbb\controller\helper $helper, \phpbb\notification\manager $notification_manager, \phpbb\pagination $pagination, \phpbb\request\request $request, \phpbb\template\template $template, \phpbb\user $user, $profile_comments_table, $profile_comments_replies_table, $root_path, $php_ext)
	{
		$this->auth = $auth;
		$this->db = $db;
		$this->helper = $helper;
		$this->notification_manager = $notification_manager;
		$this->pagination = $pagination;
		$this->request = $request;
		$this->template = $template;
		$this->user = $user;

		$this->pc_table = $profile_comments_table;
		$this->pc_replies_table = $profile_comments_replies_table;

		$this->root_path = $root_path;
		$this->php_ext = $php_ext;

		$this->user->add_lang_ext('forumpromotion/profilecomments', 'profilecomments_manage');
	}

	/**
	* Listens for core event calls.
	*
	* @return array   Array containing subscribed core events associated with callbacks. 
	*/
	static public function getSubscribedEvents()
	{
		return array(
			'core.memberlist_view_profile'          => 'prepare_display',
		);
	}

	/**
	* Handles essential profile comment logic.
	*
	* @var $event  Contextual data.
	*/
	public function prepare_display($event)
	{
		$errors = array();
		$member = $event['member'];

		// Pagination stuff.
		$start_item = (int) $this->request->variable('pcstart', 0);
		$items_per_page = 10;
		$pagination_url = append_sid($this->root_path . 'memberlist.' . $this->php_ext, 'mode=viewprofile&u=' . $member['user_id']);

		if($this->request->is_set_post('submit'))
		{
			// Pull parsed content out of message.
			$message = $this->request->variable('message', '');
			$bb_uid = $bb_bitfield = '';
			$flags = 0;
			generate_text_for_storage($message, $bb_uid, $bb_bitfield, $flags, TRUE, TRUE, TRUE);

			// User posted a comment.
			if($this->auth->acl_get('u_procom_post'))
			{
				$new_post_data = array(
					'comment_user_id_from'  => $this->user->data['user_id'],
					'comment_user_id_to'    => $member['user_id'],
					'comment_message'       => $message,
					'bbcode_bitfield'       => $bb_bitfield,
					'bbcode_uid'            => $bb_uid,
					'comment_time'          => time(),
				);
				$sql = 'INSERT INTO ' . $this->pc_table .'
				 	' . $this->db->sql_build_array('INSERT', $new_post_data);
				$this->db->sql_query($sql);

				if($member['user_id'] != $this->user->data['user_id'])
				{
					$this->notification_manager->add_notifications(
						array('forumpromotion.profilecomments.notification.type.comment'), 
						array(
							'user_id_to'     => $member['user_id'],
							'user_id_from'   => $this->user->data['user_id'],
							'message'        => (strlen($message) > 13) ? substr($message,0,10).'...' : $message
						)
					);
				}
			}
			else
			{
				$errors[] = $this->user->lang('PROCOM_NO_POST_PERMISSION');
			}
		}

		// Replies to comments.
		if($this->request->is_set_post('reply'))
		{
			$message = $this->request->variable('message', '');
			$comment_id = $this->request->variable('comment_id', 0);
			$user_from = $this->user->data['user_id'];

			// Check that the comment actually exists.
			$sql_ary = array(
				'SELECT' => 'p.comment_id, p.comment_user_id_from',
				'FROM' => array($this->pc_table => 'p'),
				'WHERE' => 'comment_id = ' . (int) $comment_id,
			);
			$sql = $this->db->sql_build_query('SELECT', $sql_ary);
			$result = $this->db->sql_query($sql);
			$comment = $this->db->sql_fetchrow($result);
			$this->db->sql_freeresult($result);

			if(!$comment)
			{
				$errors[] = $this->user->lang('PROCOM_MESSAGE_NOT_EXISTS');
			}

			// Does the user have permission to post?
			if(!$this->auth->acl_get('u_procom_post'))
			{
				$errors[] = $this->user->lang('PROCOM_NO_POST_PERMISSION');
			}

			if(empty($errors))
			{
				$bb_uid = $bb_bitfield = '';
				$flags = 0;
				generate_text_for_storage($message, $bb_uid, $bb_bitfield, $flags, TRUE, TRUE, TRUE);
				
				$new_reply_data = array(
					'comment_id'         => $comment_id,
					'reply_user_id_from' => $user_from,
					'reply_message'      => $message,
					'bbcode_bitfield'    => $bb_bitfield,
					'bbcode_uid'         => $bb_uid,
					'reply_time'         => time()
				);

				$sql = 'INSERT INTO ' . $this->pc_replies_table .'
				 	' . $this->db->sql_build_array('INSERT', $new_reply_data);
				$this->db->sql_query($sql);

				$this->notification_manager->add_notifications(
						array('forumpromotion.profilecomments.notification.type.reply'), 
						array(
							'profile_user_id' => $member['user_id'],
							'comment_id' => $comment_id,
							'comment_owner_user_id' => $comment['comment_user_id_from'],
							'user_id_from'   => $this->user->data['user_id'],
							'message'        => (strlen($message) > 13) ? substr($message,0,10).'...' : $message
						)
					);
			}
		}

		// Grab users' comments.

		$sql_ary = array(
			'SELECT'    => 'pc.*, u.username, u.user_colour, u.user_avatar, u.user_avatar_type, u.user_avatar_width, u.user_avatar_height',
			'FROM'      => array(
				$this->pc_table => 'pc',
			),
			'LEFT_JOIN' => array(
				array(
					'FROM' => array(USERS_TABLE => 'u'),
					'ON' => 'u.user_id = pc.comment_user_id_from'
				)
			),
			'WHERE'     => 'pc.comment_user_id_to = ' . $member['user_id'] . ' '
		);

		// If user shouldn't see them, exclude soft-deleted posts.
		if(!$this->auth->acl_get('m_procom_view_soft_del'))
		{
			$sql_ary['WHERE'] .= 'AND comment_soft_deleted = 0 ';
		}

		$sql_ary['ORDER_BY'] = 'pc.comment_time DESC';
		$sql = $this->db->sql_build_query('SELECT', $sql_ary);
		$result = $this->db->sql_query_limit($sql, $items_per_page, $start_item);

		$delete_url = '';
		$restore_url = '';
		$edit_url = '';
		$raw_grab_url = '';
		$soft_delete_perm = $this->auth->acl_get('m_procom_soft_del');
		$edit_others_perm = $this->auth->acl_get('m_procom_edit');

		// Get all the comments from the result and hand to template block var.
		while($row = $this->db->sql_fetchrow($result))
		{
			$from_username = get_username_string('full', $row['comment_user_id_from'], $row['username'], $row['user_colour']);

			$message = generate_text_for_display($row['comment_message'], $row['bbcode_uid'], $row['bbcode_bitfield'], 7);

			$can_soft_delete = ($soft_delete_perm || ($this->user->data['user_id'] == $row['comment_user_id_from']));
			if($can_soft_delete)
			{
				$restore_url = $this->helper->route('fp_profilecomments_restore_controller', array('comment_id' => $row['comment_id']));
				$delete_url = $this->helper->route('fp_profilecomments_delete_controller', array('comment_id' => $row['comment_id']));
			}

			$can_edit = ($edit_others_perm || ($this->user->data['user_id'] == $row['comment_user_id_from']));
			if($can_edit)
			{
				$edit_url = $this->helper->route('fp_profilecomments_edit_controller', array('comment_id' => $row['comment_id']));
			}

			$raw_grab_url = $this->helper->route('fp_profilecomments_get_raw_message_controller', array('comment_id' => $row['comment_id']));
			$avatar = get_user_avatar($row['user_avatar'], $row['user_avatar_type'], $row['user_avatar_width'], $row['user_avatar_height']);

			$this->template->assign_block_vars('profile_comments', array(
				'ID'            => $row['comment_id'],
				'AVATAR'        => $avatar,
				'USER_ID_FROM'  => $row['comment_user_id_from'],
				'USERNAME_FROM' => $from_username,
				'MESSAGE'       => $message,
				'SOFT_DELETED'  => ($row['comment_soft_deleted'] == 1),
				'TIME'          => $this->user->format_date($row['comment_time']),
				'CAN_S_DEL'     => $can_soft_delete,
				'U_S_DEL'       => ($can_soft_delete) ? $delete_url : '',
				'U_S_RESTORE'   => ($can_soft_delete) ? $restore_url : '',
				'CAN_S_EDIT'    => $can_edit,
				'U_S_EDIT'      => ($can_edit) ? $edit_url : '',
				'U_S_RAW'       => $raw_grab_url,
			));

			// Get associated comments.
			$sql_repl_ary = array(
				'SELECT' => 'r.*, u.username, u.user_colour',
				'FROM' => array($this->pc_replies_table => 'r'),
				'LEFT_JOIN' => array(
					array(
						'FROM' => array(USERS_TABLE => 'u'),
						'ON' => 'u.user_id = r.reply_user_id_from'
					)
				),
				'WHERE' => 'r.comment_id = ' . (int) $row['comment_id'],
			);

			if(!$this->auth->acl_get('m_procom_view_soft_del'))
			{
				$sql_repl_ary['WHERE'] .= ' AND reply_soft_deleted = 0 ';
			}

			$sql = $this->db->sql_build_query('SELECT', $sql_repl_ary);
			$reply_result = $this->db->sql_query($sql);

			while($reply_row = $this->db->sql_fetchrow($reply_result))
			{
				$reply_from_username = get_username_string('full', $reply_row['reply_user_id_from'], $reply_row['username'], $reply_row['user_colour']);
				$reply_message = generate_text_for_display($reply_row['reply_message'], $reply_row['bbcode_uid'], $reply_row['bbcode_bitfield'], 7);

				// Use when functionality for soft deleting replies exists.
				
				$my_uid = $this->user->data['user_id'];

				$can_soft_delete = (
					$soft_delete_perm 
					|| ($my_uid == $reply_row['reply_user_id_from'])
					|| ($my_uid == $row['comment_user_id_from'])
					|| ($my_uid == $row['comment_user_id_to'])
				);
				if($can_soft_delete)
				{
					$restore_url = $this->helper->route('fp_profilecomments_restore_reply_controller', array('reply_id' => $reply_row['reply_id']));
					$delete_url = $this->helper->route('fp_profilecomments_delete_reply_controller',  array('reply_id' => $reply_row['reply_id']));
				}
				

				$this->template->assign_block_vars('profile_comments.replies', array(
					'ID'                 => $reply_row['comment_id'],
					'USER_ID_FROM'       => $reply_row['reply_user_id_from'],
					'USERNAME_FROM'      => $reply_from_username,
					'MESSAGE'            => $reply_message,
					'SOFT_DELETED'       => ($reply_row['reply_soft_deleted'] == 1),
					'TIME'               => $this->user->format_date($reply_row['reply_time']),
					'CAN_S_DEL'     => $can_soft_delete,
					'U_S_DEL'       => ($can_soft_delete) ? $delete_url : '',
					'U_S_RESTORE'   => ($can_soft_delete) ? $restore_url : '',
		        ));
			}

			$this->db->sql_freeresult($reply_result);
		}

		$this->db->sql_freeresult($result);

		if(count($errors) > 0)
		{
			foreach($errors as $key => $value)
			{
				$this->template->assign_block_vars('procom_errors', array(
					'MESSAGE' => $value
				));
			}
		}

		// Modify previous query to get comment count.
		$sql_ary['SELECT'] = 'COUNT(pc.comment_id) as total_comments';
		$sql_ary['LEFT_JOIN'] = $sql_ary['ORDER_BY'] = NULL;
		$sql = $this->db->sql_build_query('SELECT', $sql_ary);
		$result = $this->db->sql_query($sql);
		$total_comments = $this->db->sql_fetchfield('total_comments');
		$this->db->sql_freeresult($result);

		$pagination = $this->pagination->generate_template_pagination(
			$pagination_url,
			'pagination',
			'pcstart',
			$total_comments,
			$items_per_page,
			$start_item
		);

		$this->template->assign_vars(array(
			'S_PROCOM_ERROR'          => (count($errors) > 0),
			'S_PROCOM_POST'           => $this->auth->acl_get('u_procom_post'),
			'S_PROCOM_VIEW'           => $this->auth->acl_get('u_procom_view'),
			'S_PROCOM_SOFT_DEL_VIEW'  => $this->auth->acl_get('m_procom_view_soft_del'),
			'S_PROCOM_SOFT_DEL'       => $soft_delete_perm,
			'S_PROCOM_HARD_DEL'       => $this->auth->acl_get('m_procom_hard_del'),
			'TOTAL_COMMENTS'          => $total_comments,
			'PAGINATION'              => $pagination,
			'PAGE_NUMBER'             => $this->pagination->on_page($total_comments, $items_per_page, $start_item)
		));
	}
}